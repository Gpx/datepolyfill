(function ( $, window, document, undefined) {
  
  var methods = {
    init: function( options) {
      return this.each(function() {
        var el = $(this),
            val = el.val(),
            min = el.attr('min'),
            max = el.attr('max'),
            polyfill = !!el.data('polyfill'),
            day, month, year, months, i, today = new Date(),
            dayVal, monthVal, yearVal,
            
            setDateValue = function ( event ) {
              var dateStr = year.val() + '-' + month.val() + '-' + day.val(),
                  dateIsValid = isValidDate( day.val(), month.val(), year.val() ),
                  select;

              if ( !!event ) {
                select = $( event.target );
                if ( dateIsValid ) {
                  select.data('current', select.val() );
                  el.val( dateStr );
                } else {
                  select.val( select.data('current') );
                  event.preventDefault();
                  event.stopPropagation();
                }
              } else {
                el.val( dateStr );
              }
            },
            
            isValidDate = function ( day, month, year ) {
              day = ~~day;
              month = ~~month;
              year = ~~year;

              var date = new Date(year, month, day);

              if ( month < 1 || month > 12 ) {
                return false;
              }

              if ( day < 1 || day > 31) {
                return false;
              }

              if ( (month === 4 || month === 6 || month === 9 || month === 11) && day > 30 ) {
                return false;
              }

              if ( month === 2 ) {
                if ( day > 29 ) {
                  return false;
                }

                if ( day > 28 && !(year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0))) {
                  return false;
                }
              }

              if ( min.getTime() && date > min ) {
                return false;
              }

              if ( max.getTime() && date < max ) {
                return false;
              }

              return true;
            };

        if ( !polyfill ) {
          el.hide(); // nascondo l'elemento originale
          
          // converto min e max in Date
          min = new Date(min);
          max = new Date(max);
          if ( min.getTime() && max.getTime() && min > max ) {
            min = max = new Date('wrong date');
          }

          // converto val in Date
          val = new Date(val);
          if ( min.getTime() && val.getTime() && val < min ) {
            val = min;
          } else if ( max.getTime() && val.getTime() && val > max ) {
            val = max;
          }

          //return;

          day = $('<select>', {'data-mini' :'true'});
          for ( i = 1; i <= 31; i++ ) {
            day.append(
              $('<option>', {
                value: ( i > 9 ) ? i : '0' + i,
                text: i
              })
            );
          }

          month = $('<select>', {'data-mini': 'true'});
          months = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'];
          for ( i = 1; i <= 12; i++ ) {
            month.append(
              $('<option>', {
                value: ( i > 9 ) ? i : '0' + i,
                text: months[ i-1 ]
              })
            );
          }

          year = $('<select>', {'data-mini': 'true'});
          for ( i = (min.getFullYear() || (today.getFullYear() - 5)); i <= (max.getFullYear() || (today.getFullYear() + 5)); i++) {
            year.append(
              $('<option>', {
                value: i,
                text: i
              })
            );
          }

          if ( val.getTime() ) {
            dayVal = (val.getDate() > 9) ? val.getDate() : '0' + val.getDate();
            day
              .val( dayVal )
              .data('current', dayVal);
            
            montVal = (val.getMonth() + 1 > 9) ? val.getMonth() + 1 : '0' + (val.getMonth() + 1);
            month
              .val( monthVal )
              .data('current', monthVal);
            
            yearVal = val.getFullYear();
            year
              .val( yearVal )
              .data('current', yearVal);
          } else {
            dayVal = day.val();
            day.data('current', dayVal);
            
            montVal = month.val();
            month.data('current', monthVal);
            
            yearVal = year.val();
            year.data('current', yearVal);
          }

          $(day).on('change', $.proxy(setDateValue, this));
          $(month).on('change', $.proxy(setDateValue, this));
          $(year).on('change', $.proxy(setDateValue, this));

          el.after(day, month, year).data('polyfill', true);

          setDateValue();
        }
      });
    }
  };

  $.fn.datePolyfill = function( method ) {

    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' + method + ' does not exist on jQuery.datePolyfill' );
    }
  
  };

})( jQuery, window, document );